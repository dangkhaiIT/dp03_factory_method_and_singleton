﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloSingleton
{
    public class SmartEngine
    {   
        private static Dictionary<string, int> cacheResult = new Dictionary<string, int>();


        public static int cộng(int a, int b)
        {
            string baiToan = String.Format("{0}+{1}", a, b);

            if (!cacheResult.ContainsKey(baiToan))
            {
                int kq = a + b;
                System.Threading.Thread.Sleep(5000);

                cacheResult.Add(baiToan, kq);
                return kq;
            }
            else
            {
                int kq = cacheResult[baiToan];
                return kq;
            }
        }
    }
}
